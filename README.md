# BASH batch rename

## Purpose

This BASH script will rename all of the files in a directory into integer
filenames of four characters, starting with `0001` while preserving their
extensions in Windows.  All target files will be within the directory this
script is enacted.

In order to get around the issue of Windows Preview not allowing a file to be
renamed, all renamed files are copied into a subdirectory, preserving the
original files in their original location.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/bash_rename-files.git

## Placement

It is suggested that this script be placed in `/usr/local/bin/` for simplicity
and ease-of-use.  Once this is done, it can be made executable by:

    $ chmod +x /usr/local/bin/rename-files

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/bash_rename-files/src/533d22f9ff22a1fcdda4569f4fa5c1456be7db24/LICENSE.txt?at=master) file for
details.

