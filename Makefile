#
# `make`			Build executable file `rename-files`
# `make clean`		Removes script file from /usr/local/bin
#

# *****************************************************************************

# Source files
#
SRCS = rename-files
OBJDIR = /usr/local/bin

UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Linux)
    COMMAND = sudo
endif
ifeq ($(UNAME_S),Darwin)
    COMMAND = sudo
endif

# *****************************************************************************

all:
	$(COMMAND) cp $(SRCS) $(OBJDIR)
	$(COMMAND) chmod +x $(OBJDIR)/$(SRCS)

# Remove program
#
clean:
	$(COMMAND)rm $(OBJDIR)/$(SRCS)

